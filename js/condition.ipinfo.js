(function (Drupal) {
  Drupal.smartContent = Drupal.smartContent || {};
  Drupal.smartContent.plugin = Drupal.smartContent.plugin || {};
  Drupal.smartContent.plugin.Field = Drupal.smartContent.plugin.Field || {};

  Drupal.smartContent.plugin.Field['ipinfo'] = function (condition) {
    let key = condition.field.pluginId.split(':')[1];
    if(!Drupal.smartContent.hasOwnProperty('ipinfo')) {
      if(!Drupal.smartContent.storage.isExpired('ipinfo')) {
        Drupal.smartContent.ipinfo = Drupal.smartContent.storage.getValue('ipinfo');
      }
      else {
        Drupal.smartContent.ipinfo = new Promise((resolve, reject) => {
          let ajaxObject = new Drupal.ajax({
            url: '/ajax/smart-content-ipinfo/lookup',
            progress: false,
            dataType: 'json',
            success: function (response, status) {
              if(response.hasOwnProperty('ip')) {
                Drupal.smartContent.storage.setValue('ipinfo', response);
              }
              resolve(response);
            }
          });
          ajaxObject.execute();
        });
      }
    }
    return Promise.resolve(Drupal.smartContent.ipinfo).then( (value) => {
      return value[key];
    });
  };

}(Drupal));
