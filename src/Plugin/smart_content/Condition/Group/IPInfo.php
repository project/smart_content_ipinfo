<?php

namespace Drupal\smart_content_ipinfo\Plugin\smart_content\Condition\Group;

use Drupal\smart_content\Condition\Group\ConditionGroupBase;

/**
 * Provides a condition group for IpStack conditions.
 *
 * @SmartConditionGroup(
 *   id = "ipinfo",
 *   label = @Translation("IPinfo")
 * )
 */
class IPInfo extends ConditionGroupBase {}
