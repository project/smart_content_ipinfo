<?php

namespace Drupal\smart_content_ipinfo\Plugin\smart_content\Condition;

use Drupal\smart_content\Condition\ConditionTypeConfigurableBase;

/**
 * Provides a IPinfo condition plugin.
 *
 * @SmartCondition(
 *   id = "ipinfo",
 *   label = @Translation("IPinfo"),
 *   group = "ipinfo",
 *   deriver = "Drupal\smart_content_ipinfo\Plugin\Derivative\IPInfoConditionDeriver"
 * )
 */
class IPInfoCondition extends ConditionTypeConfigurableBase {

  /**
   * {@inheritDoc}
   */
  public function getLibraries() {
    return array_unique(array_merge(parent::getLibraries(), ['smart_content_ipinfo/condition.ipinfo']));
  }

}
