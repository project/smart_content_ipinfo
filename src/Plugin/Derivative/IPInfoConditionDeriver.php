<?php

namespace Drupal\smart_content_ipinfo\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides condition plugin definitions for IPinfo fields.
 *
 * @see Drupal\smart_content_ipinfo\Plugin\smart_content\Condition\IPInfoCondition
 */
class IPInfoConditionDeriver extends DeriverBase {

  use StringTranslationTrait;

  /**
   * {@inheritDoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $this->derivatives = [
      'ip' => [
        'label' => $this->t('IP'),
        'type' => 'textfield',
      ] + $base_plugin_definition,
      'hostname' => [
        'label' => $this->t('Host name'),
        'type' => 'textfield',
      ] + $base_plugin_definition,
      'org' => [
        'label' => $this->t('Organization'),
        'type' => 'textfield',
      ] + $base_plugin_definition,
      'city' => [
        'label' => $this->t('City'),
        'type' => 'textfield',
      ] + $base_plugin_definition,
      'region' => [
        'label' => $this->t('Region'),
        'type' => 'textfield',
      ] + $base_plugin_definition,
      'country' => [
        'label' => $this->t('Country code'),
        'type' => 'textfield',
      ] + $base_plugin_definition,
      'country_name' => [
        'label' => $this->t('Country name'),
        'type' => 'textfield',
      ] + $base_plugin_definition,
      'postal' => [
        'label' => $this->t('Postal code'),
        'type' => 'textfield',
      ] + $base_plugin_definition,
      'timezone' => [
        'label' => $this->t('Timezone'),
        'type' => 'textfield',
      ] + $base_plugin_definition,
      'latitude' => [
        'label' => $this->t('Latitude'),
        'type' => 'number',
      ] + $base_plugin_definition,
      'longitude' => [
        'label' => $this->t('Longitude'),
        'type' => 'number',
      ] + $base_plugin_definition,
    ];
    return $this->derivatives;
  }

}
