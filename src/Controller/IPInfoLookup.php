<?php

namespace Drupal\smart_content_ipinfo\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\key\KeyRepositoryInterface;
use ipinfo\ipinfo\IPinfo;
use ipinfo\ipinfo\IPinfoException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Gets current IP details from IPinfo API.
 */
class IPInfoLookup extends ControllerBase {

  /**
   * IPinfo client.
   *
   * @var \ipinfo\ipinfo\IPinfo
   */
  protected $ipInfoClient;

  /**
   * The current request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $request;

  /**
   * Logger channel for this module.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * IPInfoLookup constructor.
   *
   * @param \Drupal\key\KeyRepositoryInterface $keyRepository
   *   Key repository service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Logger channel factory service.
   */
  public function __construct(KeyRepositoryInterface $keyRepository, RequestStack $requestStack, LoggerChannelFactoryInterface $loggerChannelFactory) {
    $access_token = $keyRepository->getKey('ipinfo_access_token')->getKeyValue();
    $this->ipInfoClient = new IPinfo($access_token, ['cache_disabled' => TRUE]);
    $this->request = $requestStack->getCurrentRequest();
    $this->logger = $loggerChannelFactory->get('smart_content');
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('key.repository'),
      $container->get('request_stack'),
      $container->get('logger.factory')
    );
  }

  /**
   * Fetches IPinfo details for current IP address.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse|null
   *   A JSON object containing the details if the API call was successful, NULL
   *   otherwise.
   */
  public function lookup(): ?AjaxResponse {
    if ($this->request->isXmlHttpRequest()) {
      $ip = $this->request->getClientIp();
      $ip = '71.205.87.206';
      try {
        $details = $this->ipInfoClient->getDetails($ip);
      }
      catch (IPinfoException $e) {
        $this->logger->error($e->getTraceAsString());
        return NULL;
      }
      return new AjaxResponse($details);
    }
    throw new AccessDeniedHttpException();
  }

}
